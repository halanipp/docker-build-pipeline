# Docker Build Pipeline

Standard GitLab pipeline for building and publishing Docker images.

Options:

- Docker-in-docker: [DockerBuildPush.gitlab-ci.yml](DockerBuildPush.gitlab-ci.yml)
- Kaniko: [Kaniko.gitlab-ci.yml](Kaniko.gitlab-ci.yml)
